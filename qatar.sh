#!/bin/bash

#[by fleiva]
cd ~/qatar
git pull 1>/dev/null
#VARIABLES GLOBALES:
DIRBASE=~/qatar
TODAY=`date +"%d-%m"`
TODAY_DIA=`date +"%d"`
TODAY_MES=`date +"%m"`
TODAY_ANIO=`date +"%Y"`
TODAY_ANIO=`date +"%Y"`
TOMORROW=`date +"%d-%m" --date='+1 day'`
TOMORROW_DIA=`date +"%d" --date='+1 day'`
TOMORROW_MES=`date +"%m" --date='+1 day'`
TOMORROW_ANIO=`date +"%Y" --date='+1 day'`
INICIO_DIA="20"
INICIO_MES="11"
INICIO_ANIO="2022"
FIN_DIA="18"
FIN_MES="12"
FIN_ANIO="2022"
############
# FUNCIONES
############

function mostrar (){
	case $1 in
        	grupos)
			echo "----------------------------------------------------"
			echo "🔸Grupo A: Qatar, Países Bajos, Senegal, Ecuador"
			echo "🔸Grupo B: Inglaterra, Estados Unidos, Irán, Gales"
			echo "🔸Grupo C: Argentina, México, Polonia, Arabia Saudita"
			echo "🔸Grupo D: Francia, Dinamarca, Túnez, Australia"
			echo "🔸Grupo E: España, Alemania, Japón, Costa Rica"
			echo "🔸Grupo F: Bélgica, Croacia, Marruecos, Canadá"
			echo "🔸Grupo G: Brasil, Suiza, Serbia, Camerún"
			echo "🔸Grupo H: Portugal, Uruguay, Corea del Sur, Ghana"
			echo "----------------------------------------------------"
        	;;
        	20-11)
			echo "----------------------------------------------------------"
			echo "🔸Qatar 0-2 Ecuador (13:00, Al Bayt Stadium) ⚽ TV Pública"
			echo "----------------------------------------------------------"
		;;
		21-11)
                        echo "--------------------------------------------------------------------------"
			echo "🔸Inglaterra 6-2 Irán (10:00, Khalifa International Stadium) ⚽ TyC Sports"
                        echo "🔸Senegal 0-2 Países Bajos (13:00, Al Thumama Stadium) ⚽ TV Pública"
			echo "🔸Estados Unidos 1-1 Gales (16:00, Ahmad Bin Ali Stadium) ⚽ TyC Sports"
                        echo "--------------------------------------------------------------------------"
                ;;
		22-11)
                        echo "------------------------------------------------------------------------------"
                        echo "🔸ARGENTINA 1-2 Arabia Saudita (7:00, Lusail Stadium) ⚽ TV Pública TyC Sports"
                        echo "🔸Dinamarca 0-0 Túnez (10:00, Education City Stadium) ⚽ TyC Sports"
                        echo "🔸México 0-0 Polonia (13:00, Stadium 974) ⚽ TyC Sports"
			echo "🔸Francia 4-1 Australia (16:00, Al Janoub Stadium) ⚽ TV Pública"
                        echo "------------------------------------------------------------------------------"
                ;;
		23-11)
                        echo "-------------------------------------------------------------------------"
                        echo "🔸Marruecos 0-0 Croacia (7:00, Al Bayt Stadium) ⚽ TyC Sports"
                        echo "🔸Alemania 1-2 Japón (10:00, Khalifa International Stadium) ⚽ TyC Sports"
                        echo "🔸España 7-0 Costa Rica (13:00, Al Thumama Stadium) ⚽ TV Pública"
                        echo "🔸Bélgica 1-0 Canadá (16:00, Ahmad Bin Ali Stadium) ⚽ TV Pública"
                        echo "-------------------------------------------------------------------------"
                ;;
		24-11)
                        echo "-------------------------------------------------------------------------"
                        echo "🔸Suiza 1-0 Camerún (7:00, Al Janoub Stadium) ⚽ TV Pública"
			echo "🔸Uruguay 0-0 Corea del Sur (10:00, Education City Stadium) ⚽ TV Pública"
			echo "🔸Portugal 3-2 Ghana (13:00, Stadium 974) ⚽ TyC Sports"
			echo "🔸Brasil 2-0 Serbia (16:00, Lusail Stadium) ⚽ TyC Sports"
			echo "-------------------------------------------------------------------------"
		;;
		25-11)
                        echo "-------------------------------------------------------------------------------"
                        echo "🔸Gales 0-2 Irán (7:00, Ahmad Bin Ali Stadium) ⚽ TyC Sports"
                        echo "🔸Qatar 1-3 Senegal (10:00, Al Thumama Stadium) ⚽ TV Pública"
                        echo "🔸Países Bajos 1-1 Ecuador (13:00, Khalifa International Stadium) ⚽ TyC Sports"
                        echo "🔸Inglaterra 0-0 Estados Unidos (16:00, Al Bayt Stadium) ⚽ TV Pública"
                        echo "-------------------------------------------------------------------------------"
                ;;
		26-11)
                        echo "--------------------------------------------------------------------------"
                        echo "🔸Túnez 0-1 Australia (7:00, Al Janoub Stadium) ⚽ TyC Sports"
                        echo "🔸Polonia 2-0 Arabia Saudita (10:00, Education City Stadium) ⚽ TyC Sports"
                        echo "🔸Francia 2-1 Dinamarca (13:00, Stadium 974) ⚽ TV Pública"
                        echo "🔸ARGENTINA 2-0 México (16:00, Lusail Stadium) ⚽ TV Pública TyC Sports"
                        echo "--------------------------------------------------------------------------"
                ;;
		27-11)
                        echo "-------------------------------------------------------------------------"
                        echo "🔸Japón 0-1 Costa Rica (7:00, Ahmad Bin Ali Stadium) ⚽ TyC Sports"
                        echo "🔸Bélgica 0-2 Marruecos (10:00, Al Thumama Stadium) ⚽ TyC Sports"
                        echo "🔸Croacia 4-1 Canadá (13:00, Khalifa International Stadium) ⚽ TV Pública"
                        echo "🔸España 1-1 Alemania (16:00, Al Bayt Stadium) ⚽ TV Pública"
                        echo "-------------------------------------------------------------------------"
                ;;
		28-11)
                        echo "-----------------------------------------------------------------------"
                        echo "🔸Camerún 3-3 Serbia (7:00, Al Janoub Stadium) ⚽ TyC Sports"
                        echo "🔸Corea del Sur 2-3 Ghana (10:00, Education City Stadium) ⚽ TyC Sports"
                        echo "🔸Brasil 1-0 Suiza (13:00, Stadium 974) ⚽ TV Pública"
                        echo "🔸Portugal 2-0 Uruguay (16:00, Lusail Stadium) ⚽ TyC Sports"
                        echo "-----------------------------------------------------------------------"
                ;;
		29-11)
                        echo "--------------------------------------------------------------------------"
                        echo "🔸Países Bajos 2-0 Qatar (12:00, Al Bayt Stadium) ⚽ TV Pública"
                        echo "🔸Ecuador 1-2 Senegal (12:00, Khalifa International Stadium) ⚽ TyC Sports"
                        echo "🔸Gales 0-3 Inglaterra (16:00, Ahmad Bin Ali Stadium) ⚽ TyC Sports"
                        echo "🔸Irán 0-1 Estados Unidos (16:00, Al Thumama Stadium) ⚽ TV Pública"
                        echo "--------------------------------------------------------------------------"
                ;;
		30-11)
                        echo "---------------------------------------------------------------------"
                        echo "🔸Túnez 1-0 Francia (12:00, Education City Stadium) ⚽ TyC Sports"
                        echo "🔸Australia 1-0 Dinamarca (12:00, Al Janoub Stadium) ⚽ TV Pública"
                        echo "🔸Polonia 0-2 ARGENTINA (16:00, Stadium 974) ⚽ TV Pública TyC Sports"
                        echo "🔸Arabia Saudita 1-2 México (16:00, Lusail Stadium) ⚽ TyC Sports"
                        echo "---------------------------------------------------------------------"
                ;;
		01-12 | 1-12)
                        echo "-----------------------------------------------------------------------"
                        echo "🔸Croacia 0-0 Bélgica (12:00, Ahmad Bin Ali Stadium) ⚽ TyC Sports"
                        echo "🔸Canadá 1-2 Marruecos (12:00, Al Thumama Stadium) ⚽ TV Pública"
                        echo "🔸Japón 2-1 España (16:00, Khalifa International Stadium) ⚽ TyC Sports"
                        echo "🔸Costa Rica 2-4 Alemania (16:00, Al Bayt Stadium) ⚽ TV Pública"
                        echo "-----------------------------------------------------------------------"
                ;;
		02-12 | 2-12)
                        echo "--------------------------------------------------------------------------"
                        echo "🔸Corea del Sur 2-1 Portugal (12:00, Education City Stadium) ⚽ TyC Sports"
                        echo "🔸Ghana 0-2 Uruguay (12:00, Al Janoub Stadium) ⚽ TV Pública"
                        echo "🔸Camerún 1-0 Brasil (16:00, Lusail Stadium) ⚽ TyC Sports"
                        echo "🔸Serbia 2-3 Suiza (16:00, Stadium 974) ⚽ TV Pública"
                        echo "--------------------------------------------------------------------------"
                ;;
		03-12 | 3-12)
                        echo "####################"
			echo "# OCTAVOS DE FINAL #"
                        echo "####################"
			echo "--------------------------------------------------------------------------------------"
			echo "🔸Paises Bajos 3-1 Estados Unidos (12.00; Khalifa International Stadium) ⚽ TV Pública"
			echo "🔸ARGENTINA 2-1 Australia (16.00; Ahmad Bin Ali Stadium) ⚽ TV Pública"
                        echo "--------------------------------------------------------------------------------------"
                ;;
		04-12 | 4-12)
                        echo "####################"
                        echo "# OCTAVOS DE FINAL #"
                        echo "####################"
                        echo "---------------------------------------------------------------"
			echo "🔸Francia 3-1 Polonia (12.00; Al Thumama Stadium) ⚽ TV Pública"
			echo "🔸Inglaterra 3-0 Senegal (16.00; Al Bayt Stadium) ⚽ TV Pública"
                        echo "---------------------------------------------------------------"
                ;;
		05-12 | 5-12)
                        echo "####################"
                        echo "# OCTAVOS DE FINAL #"
                        echo "####################"
                        echo "------------------------------------------------------------------"
			echo "🔸Japón (1)1-1(3) Croacia (12.00; Al Janoub Stadium) ⚽ TyC Sports"
			echo "🔸Brasil 4-1 Corea del Sur (16.00; Stadium 974) ⚽ TyC Sports"
                        echo "------------------------------------------------------------------"
                ;;
		06-12 | 6-12)
                        echo "####################"
                        echo "# OCTAVOS DE FINAL #"
                        echo "####################"
                        echo "--------------------------------------------------------------------------"
			echo "🔸Marruecos (3)0-0(0) España (12.00; Education City Stadium) ⚽ TyC Sports"
			echo "🔸Portugal 6-1 Suiza (16:00; Lusail Stadium) ⚽ TyC Sports"
                        echo "--------------------------------------------------------------------------"
                ;;
		07-12 | 7-12)
                        echo "###################"
                        echo "# NO HAY PARTIDOS #"
                        echo "###################"
                ;;
		08-12 | 8-12)
                        echo "###################"
                        echo "# NO HAY PARTIDOS #"
                        echo "###################"
                ;;
		09-12 | 9-12)
                        echo "####################"
                        echo "# CUARTOS DE FINAL #"
                        echo "####################"
                        echo "------------------------------------------------------------------------"
			echo "🔸Croacia (4)1-1(2) Brasil (12:00; Education City Stadium) ⚽ TV Pública"
			echo "🔸Paises Bajos (3)2-2(4) ARGENTINA (16.00; Lusail Stadium) ⚽ TV Pública"
                        echo "------------------------------------------------------------------------"
                ;;
		10-12)
                        echo "####################"
                        echo "# CUARTOS DE FINAL #"
                        echo "####################"
                        echo "------------------------------------------------------------------"
                        echo "🔸Marruecos 1-0 Portugal (12.00; Al Thumama Stadium) ⚽ TyC Sports"
                        echo "🔸Inglaterra 1-2 Francia (16:00; Al Bayt Stadium) ⚽ TyC Sports"
                        echo "------------------------------------------------------------------"
                ;;
		11-12)
                        echo "###################"
                        echo "# NO HAY PARTIDOS #"
                        echo "###################"
                ;;
		12-12)
                        echo "###################"
                        echo "# NO HAY PARTIDOS #"
                        echo "###################"
                ;;
		13-12)
                        echo "###############"
                        echo "# SEMIFINALES #"
                        echo "###############"
                        echo "-------------------------------------------------------------"
                        echo "🔸ARGENTINA 3-0 Croacia (16.00, Lusail Stadium) ⚽ TV Pública"
                        echo "-------------------------------------------------------------"
                ;;
		14-12)
                        echo "###############"
                        echo "# SEMIFINALES #"
                        echo "###############"
                        echo "--------------------------------------------------------------"
			echo "🔸Francia 2-0 Marruecos (16.00, Al Bayt Stadium) ⚽ TV Pública"
                        echo "--------------------------------------------------------------"
                ;;
		15-12)
                        echo "###################"
                        echo "# NO HAY PARTIDOS #"
                        echo "###################"
                ;;
                16-12)
                        echo "###################"
                        echo "# NO HAY PARTIDOS #"
                        echo "###################"
                ;;
		17-12)
                        echo "#######################"
                        echo "# 🥉 TERCER PUESTO 🥉 #"
                        echo "#######################"
                        echo "----------------------------------------------------------------------------"
                        echo "🔸Croacia 2-1 Marruecos (12:00, Khalifa International Stadium) ⚽ TyC Sports"
                        echo "----------------------------------------------------------------------------"
                ;;
		18-12)
                        echo "###############"
                        echo "# 🏆 FINAL 🏆 #"
                        echo "###############"
                        echo "-------------------------------------------------------------------"
			echo "🔸ARGENTINA (4)3-3(2) Francia (12:00, Lusail Stadium) ⚽ TV Pública"
                        echo "-------------------------------------------------------------------"
                ;;
		*)
			echo ""
			echo "NO HAY RESULTADOS PARA EL PARÁMETRO INGRESADO" | lolcat
			echo "Los parámetros posibles son:"
			echo "🔹 grupos"
			echo "🔹 fechas entre 20-11 y 18-12"
			echo "🔹 hoy"
			echo "🔹 mañana"
        	;;
	esac
}

################
#INICIO PROGRAMA
################

case $1 in
	hoy)
		if [[ ${TODAY_ANIO#0} -le ${INICIO_ANIO#0} && ${TODAY_MES#0} -le ${INICIO_MES#0} && ${TODAY_DIA#0} -lt ${INICIO_DIA#0} ]] #el doble corchete "[[" asume valores octales y falla con números mayores que 7. Para evitar este error se le agrega el numeral cero a las variables.
		then
			echo "Para un poco manija... Hasta el 20-11 no arranca esto" | lolcat
		elif [[ ${TODAY_ANIO#0} -ge ${FIN_ANIO#0} && ${TODAY_MES#0} -ge ${FIN_MES#0} && ${TODAY_DIA#0} -gt ${FIN_DIA#0} ]]
		then
			echo "🏆ARGENTINA CAMPEON!!⭐⭐⭐" | lolcat
		else
		mostrar $TODAY
		fi
	;;
	mañana)
		if [[ ${TOMORROW_ANIO#0} -le ${INICIO_ANIO#0} && ${TOMORROW_MES#0} -le ${INICIO_MES#0} && ${TOMORROW_DIA#0} -lt ${INICIO_DIA#0} ]]
                then
                        echo "Para un poco manija... Hasta el 20-11 no arranca esto" | lolcat
                elif [[ ${TOMORROW_ANIO#0} -ge ${FIN_ANIO#0} && ${TOMORROW_MES#0} -ge ${FIN_MES#0} && ${TOMORROW_DIA#0} -gt ${FIN_DIA#0} ]]
                then
                        echo "🏆ARGENTINA CAMPEON!!⭐⭐⭐" | lolcat
                else
                mostrar $TOMORROW
		fi
        ;;
	*)
		mostrar $1
	;;
esac

